from pyfirmata import Arduino, util
from tkinter import *
import time
from PIL import Image , ImageTk

placa = Arduino ('COM3')
it = util.Iterator(placa)
it.start()

led1= placa.get_pin('d:8:o') 
led2= placa.get_pin('d:9:o') 
led3= placa.get_pin('d:10:o') 
led4= placa.get_pin('d:11:o') 
led5= placa.get_pin('d:12:o') 
led6= placa.get_pin('d:13:o') 
led7= placa.get_pin('d:6:p')

sizex_circ=50
sizey_circ=50

canvas_width =  1120
canvas_height = 420

ventana = Tk()
ventana.title("BIENVENIDOS")
ventana.geometry('1200x500')
ventana.configure(bg='pale turquoise')

#VARIABLES#

led=StringVar()
est=StringVar()
content=StringVar()
cod=StringVar()

###############################################################################################
def led_pwm(valor):
    dato_lum=int(valor)

    led7.write(dato_lum/100)

    if(dato_lum>90):
        draw.itemconfig(led7_draw, fill="blue3")
        
    if(dato_lum>80 and dato_lum<=90):
        draw.itemconfig(led7_draw, fill="blue2")

    if(dato_lum>60 and dato_lum<=80):
        draw.itemconfig(led7_draw, fill="blue1")

    if(dato_lum>40 and dato_lum<=60):
        draw.itemconfig(led7_draw, fill="SkyBlue3")

    if(dato_lum>20 and dato_lum<=40):
        draw.itemconfig(led7_draw, fill="SkyBlue2")

    if (dato_lum<=20) :
        draw.itemconfig(led7_draw, fill="SkyBlue1")

def bateria (valor):
    global led8_draw
    global led9_draw
    global led10_draw
    global led11_draw
    global led12_draw
    global led13_draw
    
    if(int(valor)<=36):

        draw.itemconfig(led8_draw, fill="SkyBlue1")
        draw.itemconfig(led9_draw, fill="white")
        draw.itemconfig(led10_draw, fill="white")
        draw.itemconfig(led11_draw, fill="white")
        draw.itemconfig(led12_draw, fill="white")
        draw.itemconfig(led13_draw, fill="white")
        ventana.update()
        
    if(int(valor)>36 and int(valor)<=72):
        
        draw.itemconfig(led8_draw, fill="SkyBlue1")
        draw.itemconfig(led9_draw, fill="SkyBlue2")
        draw.itemconfig(led10_draw, fill="white")
        draw.itemconfig(led11_draw, fill="white")
        draw.itemconfig(led12_draw, fill="white")
        draw.itemconfig(led13_draw, fill="white")
        ventana.update()

    if(int(valor)>72 and int(valor)<=108):

        
        draw.itemconfig(led8_draw, fill="SkyBlue1")
        draw.itemconfig(led9_draw, fill="SkyBlue2")
        draw.itemconfig(led10_draw, fill="SkyBlue3")
        draw.itemconfig(led11_draw, fill="white")
        draw.itemconfig(led12_draw, fill="white")
        draw.itemconfig(led13_draw, fill="white")
        ventana.update()

    if(int(valor)>108 and int(valor)<=144):

        
        draw.itemconfig(led8_draw, fill="SkyBlue1")
        draw.itemconfig(led9_draw, fill="SkyBlue2")
        draw.itemconfig(led10_draw, fill="SkyBlue3")
        draw.itemconfig(led11_draw, fill="blue1")
        draw.itemconfig(led12_draw, fill="white")
        draw.itemconfig(led13_draw, fill="white")
        ventana.update()

    if(int(valor)>144 and int(valor)<=180):

        
        draw.itemconfig(led8_draw, fill="SkyBlue1")
        draw.itemconfig(led9_draw, fill="SkyBlue2")
        draw.itemconfig(led10_draw, fill="SkyBlue3")
        draw.itemconfig(led11_draw, fill="blue1")
        draw.itemconfig(led12_draw, fill="blue2")
        draw.itemconfig(led13_draw, fill="white")
        ventana.update()
        
    if(int(valor)>180):

        draw.itemconfig(led8_draw, fill="SkyBlue1")
        draw.itemconfig(led9_draw, fill="SkyBlue2")
        draw.itemconfig(led10_draw, fill="SkyBlue3")
        draw.itemconfig(led11_draw, fill="blue1")
        draw.itemconfig(led12_draw, fill="blue2")
        draw.itemconfig(led13_draw, fill="blue3")
        ventana.update()
    
def borrar():
    led.set('')
    est.set('')

def entrada():

    content.set(led.get())
    cod.set(est.get())
    
    if content.get()== "1" and (cod.get()=="on" or cod.get()=="ON") :
        led6.write(1)
        draw.itemconfig(led1_draw, fill="red")
        ventana.update()

    if content.get()== "1" and (cod.get()=="off" or cod.get()=="OFF") :
        led6.write(0)
        draw.itemconfig(led1_draw, fill="white")
        ventana.update()

    if content.get()== "2" and (cod.get()=="on" or cod.get()=="ON"):
        led5.write(1)
        draw.itemconfig(led2_draw, fill="yellow")
        ventana.update()

    if content.get()== "2" and (cod.get()=="off" or cod.get()=="OFF") :
        led5.write(0)
        draw.itemconfig(led2_draw, fill="white")
        ventana.update()

    if content.get()== "3" and (cod.get()=="on" or cod.get()=="ON"):
        led4.write(1)
        draw.itemconfig(led3_draw, fill="yellow")
        ventana.update()
        
    if content.get()== "3" and (cod.get()=="off" or cod.get()=="OFF") :
        led4.write(0)
        draw.itemconfig(led3_draw, fill="white")
        ventana.update()

    if content.get()== "4" and (cod.get()=="on" or cod.get()=="ON"):
        led3.write(1)
        draw.itemconfig(led4_draw, fill="green")
        ventana.update()

    if content.get()== "4" and (cod.get()=="off" or cod.get()=="OFF") :
        led3.write(0)
        draw.itemconfig(led4_draw, fill="white")
        ventana.update()
  
    if content.get()== "5" and (cod.get()=="on" or cod.get()=="ON"):
        led2.write(1)
        draw.itemconfig(led5_draw, fill="green")
        ventana.update()
    if content.get()== "5" and (cod.get()=="off" or cod.get()=="OFF") :
        led2.write(0)
        draw.itemconfig(led5_draw, fill="white")
        ventana.update()

    if content.get()=="6" and (cod.get()=="on" or cod.get()=="ON"):
        led1.write(1)
        draw.itemconfig(led6_draw, fill="yellow")
        ventana.update()
    if content.get()== "6" and (cod.get()=="off" or cod.get()=="OFF") :
        led1.write(0)
        draw.itemconfig(led6_draw, fill="white")
        ventana.update()
        
    borrar()
    
###########################################################################

draw = Canvas(ventana, width=canvas_width, height=canvas_height,bg="white")
draw.place(x=40,y=40)

led1_draw=draw.create_oval(610,10,610+sizex_circ,10+sizey_circ,fill="white")
led2_draw=draw.create_oval(670,10,670+sizex_circ,10+sizey_circ,fill="white")
led3_draw=draw.create_oval(730,10,730+sizex_circ,10+sizey_circ,fill="white")
led4_draw=draw.create_oval(790,10,790+sizex_circ,10+sizey_circ,fill="white")
led5_draw=draw.create_oval(850,10,850+sizex_circ,10+sizey_circ,fill="white")
led6_draw=draw.create_oval(910,10,910+sizex_circ,10+sizey_circ,fill="white")
led7_draw=draw.create_oval(730,180,730+sizex_circ,180+sizey_circ,fill="white")

led8_draw=draw.create_oval(610,300,610+sizex_circ,300+sizey_circ,fill="white")
led9_draw=draw.create_oval(670,300,670+sizex_circ,300+sizey_circ,fill="white")
led10_draw=draw.create_oval(730,300,730+sizex_circ,300+sizey_circ,fill="white")
led11_draw=draw.create_oval(790,300,790+sizex_circ,300+sizey_circ,fill="white")
led12_draw=draw.create_oval(850,300,850+sizex_circ,300+sizey_circ,fill="white")
led13_draw=draw.create_oval(910,300,910+sizex_circ,300+sizey_circ,fill="white")
####################################################################

titulo=Label(ventana,text="PUNTO 3",font=("Rockwell Extra Bold",18),fg="black")
titulo.pack()
titulo.configure(bg='pale turquoise')

#########################################################################

label1=Label(ventana,text="ELIJA UN LED: ",font=("Rockwell Extra Bold",14),fg="black")
label1.place(x=60,y=60)
label1.configure(bg='pale turquoise')

marco1 = Frame(ventana, bg="green", width=300, height=100, bd= 5)
marco1.place(x = 360,y = 60)

eleccion=Entry(marco1,width=20,justify=CENTER,textvariable=led)
eleccion.pack()


##################################################################

label2=Label(ventana,text="ESTADO DEL LED: ",font=("Rockwell Extra Bold",14),fg="black")
label2.place(x=60,y=110)
label2.configure(bg='pale turquoise')

marco2 = Frame(ventana, bg="red", width=300, height=100, bd= 5)
marco2.place(x = 360,y = 110)

estado=Entry(marco2,width=20,justify=CENTER,textvariable=est)
estado.pack()

#####################################################

start=Button(ventana,text="ENVIAR",command=entrada)
start.place(x=370,y=160)
start.config(width=15, height=2)

#####################################################

lum = Scale(ventana, 
            command=led_pwm, 
            from_= 1, 
            to = 100, 
            orient = 
            HORIZONTAL, 
            length = 300, 
            label = "INTENSIDAD",
            bg = 'maroon1',
            font=("Arial Bold", 15),
            fg="white"

            )
lum.place(x=60,y=220)

lum2 = Scale(ventana, 
            command=bateria, 
            from_= 1, 
            to = 216, 
            orient = 
            HORIZONTAL, 
            length = 300, 
            label = "BATERIA",
            bg = 'blue1',
            font=("Arial Bold", 15),
            fg="white"

            )
lum2.place(x=60,y=320)

ventana.mainloop()
